<?php


add_action('wp_ajax_exob_addSendSite', 'exob_addSendSite');
function exob_addSendSite() {

	extract($_POST);

	$sendPageLoadSettings = get_option('exob_sendSettings');
	if (!$sendPageLoadSettings) $sendPageLoadSettings = array(
		"sites" => array()
	);
	$sendPageLoadSettings["sites"][] = array(
		"title" => $title,
		"url" => $url,
		"categories" => $categories,
		"secret" => $secret,
	);
	update_option('exob_sendSettings', $sendPageLoadSettings);

	echo "success";

	die();
}

add_action('wp_ajax_exob_addGetSite', 'exob_addGetSite');
function exob_addGetSite() {

	extract($_POST);

	$exobGetSettings = get_option('exob_getSettings');
	if (!$exobGetSettings) $exobGetSettings = array(
		"sites" => array()
	);
	$exobGetSettings["sites"][] = array(
		"title" => $title,
		"url" => $url,
		"categories" => $categories,
		"secret" => $secret,
	);
	update_option('exob_getSettings', $exobGetSettings);

	echo "success";

	die();
}

add_action('wp_ajax_exob_saveGetSites', 'exob_saveGetSites');
function exob_saveGetSites() {
	$exobGetSettings = get_option('exob_getSettings');
	if (!$exobGetSettings) $exobGetSettings = array(
		"sites" => array()
	);

	$exobGetSettings["sites"] = $_POST["sites"];
	update_option('exob_getSettings', $exobGetSettings);

	echo "success";

	die();
}

add_action('wp_ajax_exob_saveGlobalSettings', 'exob_saveGlobalSettings');
function exob_saveGlobalSettings() {
	$exobGlobalSettings = get_option('exob_globalSettings');
	$exobGlobalSettings["metaSourceNameKey"] = $_POST["metaSourceNameKey"];
	$exobGlobalSettings["metaSourceUrlKey"] = $_POST["metaSourceUrlKey"];
	$exobGlobalSettings["postsAuthor"] = $_POST["postsAuthor"];
	$exobGlobalSettings["randomAuthor"] = $_POST["randomAuthor"] == "true";
	$exobGlobalSettings["randomAuthors"] = $_POST["randomAuthors"];
	update_option('exob_globalSettings', $exobGlobalSettings);
    exob_update();
	echo "success";

	die();
}

add_action('wp_ajax_exob_getAuthorsList', 'exob_getAuthorsList');
function exob_getAuthorsList() {

	if($_POST["search"]=="") {
		$users = get_users(array(
			'orderby' => 'nicename',
			'fields' => ['ID', 'user_nicename'],
			'exclude' => $_POST["selected"]
		));
    } else {
		$users = get_users([
			"exclude" => $_POST["selected"],
			"search" => $_POST["search"]
		]);
    }
	if (isset($_POST["json"]) && $_POST["json"]) {
		$out = array();
		foreach ($users as $user) {
			$out[] = [
				"ID" => $user->ID,
				"nickname" => $user->user_nicename
			];
		}
		echo json_encode($out);
		die();
	}
	foreach ($users as $user) : ?>
        <div>
            <span class="authorNickname"
                  idAuthor="<?php echo $user->ID; ?>"><?php echo $user->user_nicename; ?></span><span class="addAuthor">+</span>
        </div>
	<?php
	endforeach;

	die();
}