<?php
require_once('../../../../wp-load.php');

if (!isset($_POST["action"]) || $_POST["action"] != 'addPost') {
	wp_die();
}

ini_set('memory_limit', '2048M');

global $wpdb;
$getSettings = get_option('exob_getSettings');
$globalSettings = exob_getGlobalSettings();
$_POST["title"] = str_replace("\\\"", "\"", $_POST["title"]);
foreach ($getSettings["sites"] as $key => $site) {
	preg_match("/https?:\/\/(.*)/ui", $site["url"], $matches);
	if (!isset($matches[1])) {
		$matches[1] = $site["url"];
		$site["url"] = $site["ip"];
	}
	if ($matches[1] == $_POST["domain"]) {
		$securityString = md5($_POST["domain"] . ';' . $site["secret"] . ';' . $_POST["title"] . ';');
		if ($securityString != $_POST["md5"]) {
			continue;
		}

		$categories = array();
		$tags = array();
		$wait = 0;
		$customAuthor = -1;
		$postCategory = trim($_POST["category"]);
		$metaSourceNameKey = $globalSettings["metaSourceNameKey"];
		$metaSourceUrlKey = $globalSettings["metaSourceUrlKey"];
		$sourceName = trim($_POST["source"]);
		foreach ($site["categories"] as $category) {
			if (trim($category["slug"]) == $postCategory) {
				$categories[] = $category["categoryId"];
				$tags = explode(",", $category["tags"]);
				foreach ($tags as $key => $tag) {
					$tag = trim($tag);
					if ($tag == "") {
						array_splice($tags, $key, 1);
					}
				}
				if (isset($category["wait"]) && $category["wait"] != "") $wait = $category["wait"];
				if (isset($category["sourceNameMeta"]) && $category["sourceNameMeta"] != "") $metaSourceNameKey = $category["sourceNameMeta"];
				if (isset($category["sourceName"]) && $category["sourceName"] != "") $sourceName = $category["sourceName"];
				if (isset($category["authorId"]) && $category["authorId"] > 0) $customAuthor = $category["authorId"];
				break;
			}
		}

		$postTitle = trim($_POST["title"]);
		$issetPosts = $wpdb->get_results($wpdb->prepare("SELECT id FROM $wpdb->posts WHERE post_title='%s'", $postTitle));
		if (count($issetPosts) != 0 && !isset($_POST["update"])) {
			die("POST_ALREADY_EXISTS");
		}
		if(isset($_POST["update"])) {
			$args = array(
				'post_type' => 'post',
				'post_parent'   => 0,
				'ignore_sticky_posts'   => 1,
				'meta_query'    => array(
					array(
						'key' => $metaSourceUrlKey,
						'value' => trim($_POST["source_url"]),
						'compare' => '=',
					)
				)
			);
			$query = new WP_Query($args);
			if(!$query->have_posts()) {
				$_POST["update"] = false;
			}
			$query->the_post();
		}

		$shortStory = $_POST["short_story"];
		$fullStory = $_POST["full_story"];
		$mainImgTag = trim($_POST["main_image"]);
		if ($mainImgTag != "") {
			if (!preg_match("/^http/ui", $mainImgTag)) {
				$mainImgTag = $site["url"] . $mainImgTag;
			}
			//$mainImgTag = exob_saveImage($mainImgTag)["file"];
		}

		$postArr = array(
			'post_type' => 'post',
			'post_status' => 'draft',
			"post_title" => $postTitle,
			"post_content" => $fullStory,
			"post_category" => $categories,
			"tags_input" => $tag,
			"meta_input" => array(
				'exob_received' => $site["title"],
				'exob_site_url'	=> $site["url"],
				'exob_need_update' => "1",
			)
		);
		if($mainImgTag != "") {
			$postArr["meta_input"]["exob_main_image"] = $mainImgTag;
		}
		if($metaSourceNameKey=="") $metaSourceNameKey = "source_name";
		if($metaSourceUrlKey=="") $metaSourceUrlKey = "source";
		$postArr["meta_input"][$metaSourceNameKey] = $sourceName;
		$postArr["meta_input"][$metaSourceUrlKey] = trim($_POST["source_url"]);

		if ($customAuthor == -1) {
			if ($globalSettings["randomAuthor"]) {
				if (count($globalSettings["randomAuthors"]) > 0) {
					$postArr["post_author"] = $globalSettings["randomAuthors"][array_rand($globalSettings["randomAuthors"])];
				}
			} else {
				if ($globalSettings["postsAuthor"] != "") {
					$postArr["post_author"] = $globalSettings["postsAuthor"];
				}
			}
		} else $postArr["post_author"] = $customAuthor;
		if (!empty($shortStory)) {
			$postArr["post_excerpt"] = $shortStory;
		}
		if ($wait != 0 && !isset($_POST["update"])) {
			if(strpos($wait, "-")!==false) {
				$min = explode("-",$wait)[0];
				$max = explode("-",$wait)[1];
				$wait = rand($min, $max);
			} else $wait = (int)filter_var($wait, FILTER_SANITIZE_NUMBER_INT);
			$postArr["post_date"] = date("Y-m-d H:i:s", current_time("timestamp") + $wait * 60);
		}


		if (isset($_POST["update"])) {
			$postArr["ID"] = $query->post->ID;
			$postArr["post_date"] = $query->post->post_date;

			remove_all_images($query->post->post_content);
			remove_all_images($query->post->post_excerpt);
			$thumbnail_id = get_post_thumbnail_id($query->post);
			if($thumbnail_id) {
				delete_post_thumbnail($query->post);
				wp_delete_attachment($thumbnail_id, true);
			}

			wp_update_post($postArr);
			//wp_schedule_single_event( time()+10, 'exob_publish_post_hook' );
		} else {
			$post_id = wp_insert_post($postArr, true);
			if (!is_wp_error($post_id)) {
				//wp_schedule_single_event( time()+10, 'exob_publish_post_hook' );
			}
		}

		echo "OK";
		die();
	}
}

echo "Error, title: " . $_POST["title"];

function remove_all_images($content) {
	preg_match_all("/<img.+?src=.['\"](.+?).['\"].*?>/uim", $content, $matches, PREG_SET_ORDER);
	foreach ($matches as $key => $img) {
		$attachment_id = attachment_url_to_postid($img[1]);
		if($attachment_id>0) {
			wp_delete_attachment($attachment_id, true);
		}
	}
}