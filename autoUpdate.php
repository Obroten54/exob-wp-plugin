<?php

$pluginPatch = plugin_dir_path(__FILE__);
$currentFile = file_get_contents($pluginPatch."exob_init.php", false, NULL, 0, 250);
preg_match("/Version: (.+?)\n/ui", $currentFile, $matches);
$currentVersion = $matches[1];

$newFile = file_get_contents("https://gitlab.com/Obroten54/exob-wp-plugin/raw/master/exob_init.php", false, NULL, 0, 250);
if (preg_match("/Version: (.+?)\n/ui", $newFile, $matches)) {
	$newVersion = $matches[1];
	if ($newVersion <= $currentVersion) return;

	$fileVersion = $pluginPatch."newVersion.zip";
	file_put_contents($fileVersion, fopen("https://gitlab.com/Obroten54/exob-wp-plugin/-/archive/master/exob-wp-plugin-master.zip", 'r'));

	$path = pathinfo(realpath($fileVersion), PATHINFO_DIRNAME);
	$zip = new ZipArchive;
	$res = $zip->open($fileVersion);
	if ($res === TRUE) {
		$zip->extractTo($path);
		$zip->close();
	}

	$extractPatch = $pluginPatch."exob-wp-plugin-master";
	exob_recurse_copy($extractPatch, $pluginPatch);
	exob_delete_directory($extractPatch);
	unlink($fileVersion);
}

function exob_recurse_copy($src,$dst) {
	$dir = opendir($src);
	@mkdir($dst);
	while(false !== ( $file = readdir($dir)) ) {
		if (( $file != '.' ) && ( $file != '..' )) {
			if ( is_dir($src . '/' . $file) ) {
				if($file==".idea") continue;
				exob_recurse_copy($src . '/' . $file,$dst . '/' . $file);
			}
			else {
				copy($src . '/' . $file,$dst . '/' . $file);
			}
		}
	}
	closedir($dir);
}

function exob_delete_directory($dirname) {
	if (is_dir($dirname))
		$dir_handle = opendir($dirname);
	/** @noinspection PhpUndefinedVariableInspection */
	while($file = readdir($dir_handle)) {
		if ($file != "." && $file != "..") {
			if (!is_dir($dirname."/".$file))
				unlink($dirname."/".$file);
			else
				exob_delete_directory($dirname.'/'.$file);
		}
	}
	closedir($dir_handle);
	rmdir($dirname);
	return true;
}