<?php
/*
Plugin Name: EXOB
Plugin URI: https://proekt-obroten.ru/
Description: Plugin for simple exchange posts between sites and get posts from Central Parser
Author: Obroten54
Version: 1.956
Author URI: https://proekt-obroten.ru/
*/

add_action('admin_menu', 'exob_menu');
function exob_menu() {
	add_menu_page("Exchange posts", "Exchange posts", "administrator", "exob_sendPosts", 'exob_sendPageLoad');

	//add_submenu_page("ex_sendPosts", "Send posts", "Send posts","administrator","ex_sendPosts","exob_sendPageLoad");
	add_submenu_page("exob_sendPosts", "Get posts", "Get posts", "administrator", "exob_getPosts", "exob_getPageLoad");
	add_submenu_page("exob_sendPosts", "Global settings", "Global settings", "administrator", "exob_globalSettings", "exob_globalSettings");
}

require_once("inc/exobSend.php");
require_once("inc/exobGet.php");
require_once("inc/exobGlobal.php");
require_once("inc/exobCron.php");

require_once("ajax/ajaxInit.php");


function exob_loadScripts($hook) {
	$resourceVersion = 6;
	if (strpos($hook, "posts_page_exob") > -1) {
		wp_enqueue_script('exob_custom_js', plugins_url('/js/exobCustom.js?v='.$resourceVersion, __FILE__), array('jquery'));

		wp_enqueue_style('exob_custom_css', plugins_url('/css/exobCustom.css?v='.$resourceVersion, __FILE__));
	}
}

add_action('admin_enqueue_scripts', 'exob_loadScripts');

add_filter('wp_kses_allowed_html', 'exob_allowIframe', 1, 1);
function exob_allowIframe($allowedposttags) {
	$allowedposttags['iframe'] = array(
		'align' => true,
		'width' => true,
		'height' => true,
		'frameborder' => true,
		'name' => true,
		'src' => true,
		'id' => true,
		'class' => true,
		'style' => true,
		'scrolling' => true,
		'marginwidth' => true,
		'marginheight' => true,
		'allowfullscreen' => true,
		'mozallowfullscreen' => true,
		'webkitallowfullscreen' => true,
	);

	return $allowedposttags;
}

if (!wp_next_scheduled('exob_update_hook'))
	wp_schedule_event(time(), 'hourly', 'exob_update_hook');
add_action('exob_update_hook', 'exob_update', 10);
function exob_update() {
	$pluginPatch = plugin_dir_path(__FILE__);
	include $pluginPatch."autoUpdate.php";
}

function exob_getGlobalSettings() {
	$exobGlobalSettings = get_option('exob_globalSettings');
	if (!$exobGlobalSettings) {
		$exobGlobalSettings = array(
			"metaSourceUrlKey" => "",
			"metaSourceNameKey" => "",
			"postsAuthor" => "",
		);
	}

	if(!isset($exobGlobalSettings["randomAuthor"])) $exobGlobalSettings["randomAuthor"]=false;
	if(!isset($exobGlobalSettings["randomAuthors"])) $exobGlobalSettings["randomAuthors"]=array(0);

	return $exobGlobalSettings;
}