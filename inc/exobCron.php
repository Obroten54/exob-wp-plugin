<?php

add_filter( 'cron_schedules', 'exob_interval');
function exob_interval( $schedules ) {
	$schedules['exob_30_seconds'] = array(
		'interval' => 30,
		'display' => 'Every 30 seconds'
	);
	return $schedules;
}

add_action( 'exob_publish_post_hook', 'exob_publish_post' );
if( !wp_next_scheduled('exob_publish_post_hook' ) )
	wp_schedule_event( time(), 'exob_30_seconds', 'exob_publish_post_hook' );
function exob_publish_post() {
	$exob_update_query_args = array(
		'post_type' => 'post',
		'meta_query' => array(
			array(
				'key' => 'exob_need_update',
				'value' => '1',
				'compare' => '=',
			)
		),
		'posts_per_page' => 4,
		"post_status"	=> 'any',
		'ignore_sticky_posts' => 1
	);
	$exob_update_query = new WP_Query();
	$exob_update_posts = $exob_update_query->query($exob_update_query_args);
	foreach ($exob_update_posts as $post) {
		delete_post_meta($post->ID, "exob_need_update");
	}
	foreach ($exob_update_posts as $post) {
		$site = get_post_meta($post->ID, "exob_site_url", true);
		$mainImage = get_post_meta($post->ID, "exob_main_image", true);
		$full_story = exob_downloadImage($post->post_content, $site);
		$short_story = exob_downloadImage($post->post_excerpt, $site);
		if($mainImage)
			$mainImage = exob_saveImage($mainImage)["file"];
		$postArr = [
			"ID"	=> $post->ID,
			"post_content" => $full_story,
			"post_excerpt" => $short_story,
			"post_date"	=> $post->post_date
		];
		if($post->post_status=="draft") {
			$postArr["post_status"] = "publish";
		}
		wp_update_post($postArr);
		if($mainImage)
			exob_generate_featured_image($mainImage, $post->ID);
		delete_post_meta($post->ID, "exob_main_image");
		delete_post_meta($post->ID, "exob_site_url");
	}
}

function exob_downloadImage($content, $site) {
	preg_match_all("/<img.+?src=.?['\"](.+?)['\"\\]?['\"].+?>/uim", $content, $matches, PREG_SET_ORDER);
	foreach ($matches as $key => $img) {
		if (!preg_match("/http/ui", $img[1])) {
			$img[1] = $site . $img[1];
		}
		$file = exob_saveImage($img[1]);
		$imgTag = '<img src="' . str_replace(site_url(), '', $file['url']) . '" />';
		$content = str_replace($img[0], $imgTag, $content);
	}

	return $content;
}

function exob_saveImage($fileUrl, $timeoutSeconds = 10) {
	if(!function_exists("download_url")) {
		require_once(exob_get_admin_dir_patch() . 'includes/file.php');
	}

	$tempFile = download_url($fileUrl, $timeoutSeconds);

	if (!is_wp_error($tempFile)) {
		$ext = strtolower(pathinfo($fileUrl, PATHINFO_EXTENSION));
		$fileName = basename(preg_replace('/\?.*/', '', $fileUrl));
		$fileName = explode(".", $fileName)[0];
		if (!$ext || mb_strpos($ext, "?") !== false) {
			$ch = curl_init($fileUrl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_exec($ch);

			$type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

			if (mb_strpos($type, "/") !== false) {
				$ext = explode('/', $type)[1];
				preg_match('/([^;\s]+)/', $ext, $matches);
				if ($matches && isset($matches[1])) {
					$ext = $matches[1];
				}
			}
		}

		$file = [
			'name' => "{$fileName}.{$ext}",
			'ext' => $ext,
			'tmp_name' => $tempFile,
			'error' => 0,
			'size' => @filesize($tempFile),
		];

		$overrides = [
			'test_form' => false,
			'test_size' => true,
			'test_upload' => true,
			'test_type' => false,
		];

		$results = wp_handle_sideload($file, $overrides);

		if (empty($results['error'])) {
			return $results;
		}
	}

	return null;
}

function exob_get_admin_dir_patch() {
	$admin_path = str_replace( get_bloginfo( 'url' ) . '/', ABSPATH, get_admin_url() );

	return $admin_path;
}

function exob_generate_featured_image($image_url, $post_id) {
	$upload_dir = wp_upload_dir();
	$image_data = file_get_contents($image_url);
	$filename = basename($image_url);
	if (wp_mkdir_p($upload_dir['path'])) $file = $upload_dir['path'] . '/' . $filename;
	else                                    $file = $upload_dir['basedir'] . '/' . $filename;
	file_put_contents($file, $image_data);

	$wp_filetype = wp_check_filetype($filename, null);
	$attachment = array(
		'post_mime_type' => $wp_filetype['type'],
		'post_title' => sanitize_file_name($filename),
		'post_content' => '',
		'post_status' => 'inherit'
	);
	$attach_id = wp_insert_attachment($attachment, $file, $post_id);
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	$attach_data = wp_generate_attachment_metadata($attach_id, $file);
	wp_update_attachment_metadata($attach_id, $attach_data);
	set_post_thumbnail($post_id, $attach_id);
}
