<?php

function exob_getPageLoad() {
	$sendPageLoadSettings = get_option('exob_getSettings');
	if (!$sendPageLoadSettings) {
		$sendPageLoadSettings = array(
			"sites" => array()
		);
	}

	echo "<h2>Принятие постов</h2>";
	echo "<p class='linkDonor'>На доноре разместите ссылку: ..."
		. substr(plugins_url("/exob/ajax/request.php"), -21) .
		" <span class='exobSuccess successCopy'>Скопировано!</span><input type='text' class='copyInput' value='" . plugins_url("/exob/ajax/request.php") . "'></p>";
	echo "<div class='sites'>";
	if (count($sendPageLoadSettings["sites"]) == 0) {
		echo "<p>Нет доноров...</p>";
	} else {
		foreach ($sendPageLoadSettings["sites"] as $key => $site) {
			echo "<div class='siteSpoiler'>";
			preg_match("/https?:\/\/(.*)/ui", $site["url"], $matches);
			if (!$matches[1]) {
				preg_match("/https?:\/\/(.*)/ui", $site["ip"], $matches);
			}
			echo "<h4>" . $site["title"] . " [" . $matches[1] . "]</h4>";
			echo "<div class='hidden'>";
			$ip = "";
			if (isset($site["ip"])) {
				$ip = $site["ip"];
			}
			exob_GetForm($site["title"], $site["categories"], $site["url"], $site["secret"], $ip);
			echo "</div>";
			echo "</div>";
		}
	}
	echo "</div>";
	echo "<div class='exobButtons'><button id='saveGetSites'>Сохранить</button>";
	echo "<button id='addGetSite'>+ Добавить новый +</button></div>";
	echo "<span class='exobSuccess successSave'>Сохранено!</span>";
	echo "<div class='exobAddForm hidden'>";
	exob_GetForm();
	echo "</div>";
}

function exob_GetForm($title = "", $sCategories = array(), $link = "", $secret = "", $ip = "") {
	$allCategories = get_categories(array(
		'hide_empty' => false,
	));
	if (count($sCategories) == 0) {
		$sCategories[] = array(
			"slug" => "",
			"categoryId" => "",
			"tags" => "",
		);
	}
	?>
    <div class="exobForm">
        <label>Название сайта:</label>
        <input type="text" name="title" placeholder="Название сайта" value="<?php echo $title; ?>">
        <label>Категории, в которых следует размещать посты:</label>
        <div class="categorySelectBox">
			<?php foreach ($sCategories as $key => $categoryS) { ?>
                <div class="exobCategoryObj">
                    <div class="exobCategoryBlock">
                        <p class="exobCategoryTitle"><?= ($categoryS["slug"] != "") ? $categoryS["slug"] : "..." ?></p>
                        <span class="removeAttr"></span>
                    </div>
                    <div class="exobCategoryOptions hide">
                        <input type="text" class="categorySlug" placeholder="Слаг принимаемой категории"
                               value="<?= $categoryS["slug"] ?>"/>
                        <select  class="categoryId">
							<?php
							foreach ($allCategories as $categoryG) {
								$selected = "";
								if ($categoryG->term_id == $categoryS["categoryId"]) {
									$selected = "selected='selected'";
								}
								echo "<option value='" . $categoryG->term_id . "' " . $selected . ">" . $categoryG->name . "</option>";
							}
							?>
                        </select>
                        <input type="text" class="categoryTag" placeholder="Тэги для категории, через запятую"
                               value="<?= $categoryS["tags"]; ?>"/>
                        <input type="text" class="categoryWait" placeholder="Откладывать публикацию на ... минут"
                               value="<?= (isset($categoryS["wait"]) && $categoryS["wait"] != "") ? $categoryS["wait"] : "" ?>"/>
                        <input type="text" class="categoryAuthorSearch" placeholder="Назначать автором..."
                               value=""/>
                        <select class="categoryAuthor">
							<?php
                            $author = (isset($categoryS["authorId"])&&$categoryS["authorId"]!="") ? $categoryS["authorId"] : "exobGlobalSettings";
                            ?>
                            <option value="" <?=($author=="exobGlobalSettings") ? "selected='selected'" : ""?>>Глобальные настройки</option>
                            <?php
                            $excludeIds = array();
                            if($author!="exobGlobalSettings") {
                                $user = get_user_by("ID", $author);
                                if($user!==false) {
									$excludeIds[] = $user->ID;
									?>
                                    <option value="<?=$user->ID?>" selected="selected"><?= $user->user_nicename ?></option>
                                    <?php
                                }
                            }
							$users = get_users(array(
								'orderby' => 'nicename',
								'fields' => ['ID', 'user_nicename'],
                                'exclude'   => $excludeIds
							));
							foreach ($users as $user) : ?>
                                <option value="<?=$user->ID?>"><?=$user->user_nicename?></option>
                            <?php endforeach;
							?>
                        </select>
                        <input type="text" class="categoryCustomSourceNameMeta" placeholder="Кастомный meta-key для названия источника"
                               value="<?=(isset($categoryS["sourceNameMeta"])) ? $categoryS["sourceNameMeta"] : ""; ?>"/>
                        <input type="text" class="categoryCustomSourceName" placeholder="Кастомное название источника"
                               value="<?=(isset($categoryS["sourceName"])) ? $categoryS["sourceName"] : ""; ?>"/>
                    </div>
                </div>
			<?php } ?>
            <button class="addAttrBlock">Добавить принимаемую категорию</button>
        </div>
        <label>URL главной страницы донора:</label>
        <input type="text" name="url" placeholder="spy.com" value="<?php echo $link; ?>">
        <label>Секретное слово (должно совпадать с секретным словом донора):</label>
        <input type="text" name="secret" placeholder="MikaPrincess" value="<?php echo $secret; ?>">
		<?php if (!$title) : ?>
            <button id='ajaxAddGetSite'>Добавить сайт</button>
		<?php else :
			if (!preg_match("/https?:\/\/(.*)/ui", $link)) {
				?>
                <label>У сайта не обнаружен домен, введите ip-адрес:</label>
                <input type="text" name="ip" placeholder="http://212.47.238.11:8080" value="<?php echo $ip; ?>">
				<?php
			}
		endif; ?>
    </div>
	<?php
}

?>
