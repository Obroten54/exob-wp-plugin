<?php

function exob_globalSettings() {
	$exobGlobalSettings = exob_getGlobalSettings();
	?>
    <h2>Глобальные настройки EXOB</h2>
    <div id="exobGlobalSettings">
        <h4>Источник постов</h4>
        <label>Meta-key для названия источника:</label>
        <input type="text" name="metaSourceNameKey" placeholder="source_name"
               value="<?php echo $exobGlobalSettings["metaSourceNameKey"]; ?>">
        <label>Meta-key для оригинальной ссылки на пост:</label>
        <input type="text" name="metaSourceUrlKey" placeholder="source"
               value="<?php echo $exobGlobalSettings["metaSourceUrlKey"]; ?>">
        <h4>Автор постов</h4>
        <div class="settingsBlock">
            <label class="inlineLabel">Назначать автора рандомно:</label>
            <label class="switch">
                <input id="postAuthorRandomCheckbox"
                       type="checkbox" <?php echo ($exobGlobalSettings["randomAuthor"]) ? "checked" : "" ?>>
                <span class="slider round"></span>
            </label>
        </div>
        <label>Назначать автором постов:</label>
        <div id="specificAuthor" class="settingsBlock <?php echo ($exobGlobalSettings["randomAuthor"]) ? "hidden" : "" ?>">
            <select name="postAuthor">
				<?php
				$users = get_users(array(
					'orderby' => 'nicename',
					'fields' => ['ID', 'user_nicename']
				));
				$selected = false;
				foreach ($users as $user) :
					if ($user->ID == $exobGlobalSettings["postsAuthor"]) {
						$selected = true;
					}
					?>
                    <option value="<?php echo $user->ID; ?>" <?php echo ($user->ID == $exobGlobalSettings["postsAuthor"]) ? "selected=\"selected\"" : ""; ?>><?php echo $user->user_nicename; ?></option>
				<?php
				endforeach;
				if (!$selected) :
					?>
                    <option value="0" selected="selected" disabled="disabled">Автор поста</option>
				<?php
				endif; ?>
            </select>
        </div>
        <div id="randomAuthors" class="settingsBlock <?php echo (!$exobGlobalSettings["randomAuthor"]) ? "hidden" : "" ?>">
            <div>
                <input type="text" id="randomAuthorNickname" placeholder="Введите никнейм или email..."/>
                <label>Выбранные авторы:</label>
            </div>
            <div>
                <div>
                    <div class="authorsList">
						<?php
						$users = get_users(array(
							'orderby' => 'nicename',
							'fields' => ['ID', 'user_nicename'],
							'exclude' => $exobGlobalSettings['randomAuthors']
						));
						foreach ($users as $user) : ?>
                            <div>
                                <span class="authorNickname"
                                      idAuthor="<?php echo $user->ID; ?>"><?php echo $user->user_nicename; ?></span><span
                                        class="addAuthor">+</span>
                            </div>
						<?php
						endforeach;
						?>
                    </div>
                    <div class="loadSpinBlock hidden"><div id="loading" class="lds-dual-ring"></div></div>
                </div>
                <div>
                    <div class="authorsList selectedAuthors">
						<?php
						$users = get_users(array(
							'include' => $exobGlobalSettings['randomAuthors']
						));
						foreach ($users as $user) : ?>
                            <div>
                                <span class="authorNickname"
                                      idAuthor="<?php echo $user->ID; ?>"><?php echo $user->user_nicename; ?></span><span
                                        class="rmAuthor">-</span>
                            </div>
						<?php
						endforeach;
						?>
                    </div>
                </div>
            </div>
        </div>
        <button id='exobSaveGlobalSettings'>Сохранить настройки</button>
        <span class='exobSuccess successSave'>Сохранено!</span>
    </div>
	<?php
}
