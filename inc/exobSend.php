<?php

function exob_sendPageLoad(){
  $sendPageLoadSettings = get_option( 'exob_sendSettings' );
  if(!$sendPageLoadSettings) $sendPageLoadSettings = array(
    "sites" => array()
  );

  echo "<h2>Отправка постов</h2>";
  echo "<div class='sites'>";
  if(count($sendPageLoadSettings["sites"])==0) echo "<p>Нет получателей...</p>";
  else {
    foreach ($sendPageLoadSettings["sites"] as $key => $site) {
      echo "<div class='siteSpoiler'>";
      preg_match("/https?:\/\/(.+?)\//ui", $site["url"], $matches);
      echo "<h4>".$site["title"];
      if($matches) {
        echo " [".$matches[1]."]";
      }
      echo "</h4>";
      echo "<div class='hidden'>";
      exob_getSendForm($site["title"], $site["categories"], $site["url"], $site["secret"]);
      echo "</div>";
      echo "</div>";
    }
  }
  echo "</div>";

  echo "<button id='saveSendSites'>Сохранить</button>";
  echo "<button id='addSendSite'>+ Добавить новый +</button>";
  echo "<div class='exobAddForm hidden'>";
  exob_getSendForm();
  echo "</div>";
}

function exob_getSendForm($title="", $categories=array(), $link="", $secret="") {
  ?>
  <div class="exobForm">
    <label>Название сайта:</label>
    <input type="text" name="title" placeholder="Название сайта" value="<?php echo $title; ?>">
    <label>Категории, посты которых следует отправлять:</label>
    <div>
      <select multiple>
      <?php
        $allCategories = get_categories(array(
          'hide_empty' => false,
        ));
        foreach ($allCategories as $key => $category) {
          $selected = "";
          if(in_array($category->term_id, $categories)) {
            $selected = "selected='selected'";
          }
          echo "<option value='".$category->term_id."' ".$selected.">".$category->name."</option>";
        }
      ?>
    </select>
    </div>
    <label>URL запроса (узнайте на доноре):</label>
    <input type="text" name="url" placeholder="https://spy.com/wp-admin/plugins/ajax/request.php" value="<?php echo $link; ?>">
    <label>Секретное слово (должно совпадать с секретным словом реципиента):</label>
    <input type="text" name="secret" placeholder="MikaPrincess" value="<?php echo $secret; ?>">
    <?php if(!$title) : ?>
      <button id='ajaxAddSendSite'>Добавить сайт</button>
    <?php endif; ?>
  </div>
  <?php
}

//add_action( 'publish_post', 'exob_postSend', 10, 2 );
function exob_postSend($ID, $post) {


  $url = 'http://server.com/path';
  $data = array('key1' => 'value1', 'key2' => 'value2');

  // use key 'http' even if you send the request to https://...
  $options = array(
      'http' => array(
          'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
          'method'  => 'POST',
          'content' => http_build_query($data)
      )
  );
  $context  = stream_context_create($options);
  $result = file_get_contents($url, false, $context);
  if ($result === FALSE) { /* Handle error */ }
}

?>
