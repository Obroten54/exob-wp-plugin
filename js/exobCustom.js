jQuery(document).ready(function ($) {

    $("#postAuthorRandomCheckbox").change(function () {
        if ($(this).is(":checked")) {
            $("#specificAuthor").addClass("hidden");
            $("#randomAuthors").removeClass("hidden");
        } else {
            $("#randomAuthors").addClass("hidden");
            $("#specificAuthor").removeClass("hidden");
        }
    });

    function updateAuthorListeners() {
        $(".authorsList:not(.selectedAuthors) > div").click(function (e) {
            e.preventDefault();
            if ($(this).find(".addAuthor").length > 0) {
                $(this).appendTo($(".authorsList.selectedAuthors"));
                $(this).find(".addAuthor").removeClass("addAuthor").addClass("rmAuthor").html("-");
            } else {
                $(this).appendTo($(".authorsList:not(.selectedAuthors)"));
                $(this).find(".rmAuthor").removeClass("rmAuthor").addClass("addAuthor").html("+");
            }
        });
    }

    updateAuthorListeners();

    let authorsReqSending = false, needAuthorReqSend = false;
    $(".loadSpinBlock").hide();
    function updateRandomAuthors() {
        if (authorsReqSending) {
            needAuthorReqSend = true;
            return;
        }
        authorsReqSending = true;
        $(".loadSpinBlock").show();
        $(".authorsList:not(.selectedAuthors)").addClass("hidden");

        let excluded = [];
        $(".authorsList.selectedAuthors > div").each(function (ind, obj) {
            excluded.push($(obj).find(".authorNickname").attr("idAuthor"));
        });
        $.post(ajaxurl, {
            action: "exob_getAuthorsList",
            selected: excluded,
            search: $("#randomAuthorNickname").val()
        }, function (data) {
            authorsReqSending = false;
            if(needAuthorReqSend) {
                needAuthorReqSend = false;
                updateRandomAuthors();
            } else {
                $(".authorsList:not(.selectedAuthors)").html(data).removeClass("hidden");
                $(".loadSpinBlock").hide();
                updateAuthorListeners();
            }
        });
    }
    $("#randomAuthorNickname").on("input", updateRandomAuthors);

    function updateCategoryAuthors(obj) {
        if (authorsReqSending) {
            needAuthorReqSend = true;
            return;
        }
        authorsReqSending = true;
        let selectObj = $(obj).next();
        selectObj.attr("disabled", "disabled");

        let excluded = [];
        if(selectObj.val()!=="") {
            excluded.push(selectObj.val());
        }
        $.post(ajaxurl, {
            action: "exob_getAuthorsList",
            selected: excluded,
            search: $(obj).val(),
            json: true
        }, function(data){
            authorsReqSending = false;
            if(needAuthorReqSend) {
                needAuthorReqSend = false;
                updateCategoryAuthors(obj);
            } else {
                let options = [];
                options.push(selectObj.find("option").eq(0).clone());
                if(excluded.length>0) {
                    options.push(selectObj.find("option:selected").eq(0).clone());
                }
                selectObj.empty();
                for(let i=0; i<options.length; i++) {
                    selectObj.append(options[i]);
                }
                for(let i=0; i<data.length; i++) {
                    let optionObj = $("<option></option>");
                    optionObj.text(data[i]["nickname"]);
                    optionObj.attr("value", data[i]["ID"]);
                    selectObj.append(optionObj);
                }
                selectObj.removeAttr("disabled");
            }
        }, "json");
    }
    $(".categoryAuthorSearch").on("input", function () {
        updateCategoryAuthors(this);
    });

    $("#addSendSite, #addGetSite").click(function (event) {
        event.preventDefault();

        $(this).slideUp(400);
        $(".exobAddForm").slideDown('400', function () {
            $(".exobAddForm input[name=title]").focus();
        });
    });
    $("#ajaxAddSendSite").click(function (event) {
        event.preventDefault();

        if ($(".exobAddForm input[name=title]").val() == "") return;

        $(this).attr("disabled", "disabled");

        var data = {
            action: "exob_addSendSite",
            title: $(".exobAddForm input[name=title]").val(),
            categories: $(".exobAddForm select").val(),
            url: $(".exobAddForm input[name=url]").val(),
            secret: $(".exobAddForm input[name=secret]").val(),
        };
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: data,
            success: function (data) {
                if (data == "success") {
                    $(".exobAddForm input").val("");
                    $(".exobAddForm select").val("");
                    $("#ajaxAddSendSite").removeAttr('disabled');
                    $(this).slideDown(400);
                    $(".exobAddForm").slideUp('400');
                }
            }
        });

    });

    $(".siteSpoiler h4").click(function (event) {
        event.preventDefault();

        var spoilerBlock = $(this).parent(".siteSpoiler");

        if (spoilerBlock.find(".opened").length > 0) {
            spoilerBlock.children('div').slideUp('400', function () {
                $(this).removeClass('opened');
            });
        } else {
            $(".siteSpoiler div.opened").slideUp(400, function () {
                $(this).removeClass('opened');
            });
            spoilerBlock.children('div').slideDown('400', function () {
                $(this).addClass('opened');
            });
        }
    });

    $(".linkDonor").click(function (event) {
        event.preventDefault();

        $(this).find("input").select();
        document.execCommand("copy");
        $(this).find('span').fadeIn(400, function () {
            setTimeout(function () {
                $(".successCopy").fadeOut(400);
            }, 10000);
        });
    });

    $("#ajaxAddGetSite").click(function (event) {
        event.preventDefault();

        if ($(".exobAddForm input[name=title]").val() === "") return;

        $(this).attr("disabled", "disabled");

        let category = [];
        $(".exobAddForm").find(".categorySelectBox > div").each(function (ind, obj) {
            const slug = $(obj).find("input.categorySlug").val().trim();
            const categoryValue = $(obj).find("select").val().trim();
            if (slug !== "" && categoryValue !== "") {
                category.push({
                    slug: slug,
                    categoryId: categoryValue,
                    tags: $(obj).find("input.categoryTag").val().trim()
                });
            }
        });

        const data = {
            action: "exob_addGetSite",
            title: $(".exobAddForm input[name=title]").val(),
            categories: category,
            url: $(".exobAddForm input[name=url]").val(),
            secret: $(".exobAddForm input[name=secret]").val(),
        };
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: data,
            success: function (data) {
                if (data == "success") {
                    $(".exobAddForm input").val("");
                    $(".exobAddForm select").val("");
                    $("#ajaxAddGetSite").removeAttr('disabled');
                    $(this).slideDown(400);
                    $(".exobAddForm").slideUp('400');
                }
            }
        });

    });

    $("#saveGetSites").click(function (event) {
        event.preventDefault();

        $(this).attr("disabled", "disabled");

        let data = {
            action: "exob_saveGetSites",
            sites: []
        };
        $(".sites .exobForm").each(function (index, el) {
            let categories = [];
            $(el).find(".categorySelectBox > div").each(function (ind, obj) {
                const slug = $(obj).find("input.categorySlug").val().trim();
                const categoryValue = $(obj).find("select.categoryId").val().trim();
                if (slug !== "" && categoryValue !== "") {
                    categories.push({
                        slug: slug,
                        categoryId: categoryValue,
                        tags: $(obj).find("input.categoryTag").val().trim(),
                        wait: $(obj).find("input.categoryWait").val().trim(),
                        authorId: $(obj).find("select.categoryAuthor").val().trim(),
                        sourceNameMeta: $(obj).find("input.categoryCustomSourceNameMeta").val().trim(),
                        sourceName: $(obj).find("input.categoryCustomSourceName").val().trim()
                    });
                }
            });

            let site = {
                title: $(el).find("input[name=title]").val(),
                categories: categories,
                url: $(el).find("input[name=url]").val(),
                secret: $(el).find("input[name=secret]").val(),
            };
            if ($(el).find("input[name=ip]").length > 0) {
                site["ip"] = $(el).find("input[name=ip]").val();
            }

            data["sites"].push(site);
        });
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: data,
            success: function (data) {
                if (data == "success") {
                    $("#saveGetSites").removeAttr('disabled');
                    $(".successSave").fadeIn(400, function () {
                        setTimeout(function () {
                            $(".successSave").fadeOut(400);
                        }, 10000);
                    });
                }
            }
        });

    });

    $(".addAttrBlock").click(function (e) {
        e.preventDefault();

        let attrBlock = $(this).prev().clone(true);
        attrBlock.find("input").val("");
        attrBlock.find("select").val("");
        attrBlock.find(".exobCategoryTitle").text("...");
        $(this).before(attrBlock);
        initializeCategoryBlocks();
    });

    initializeCategoryBlocks();
    function initializeCategoryBlocks() {
        $(".categorySlug").on("input", function(){
            let titleObj = $(this).parents(".exobCategoryObj").find(".exobCategoryTitle");
            if($(this).val().trim()!=="")
                titleObj.text($(this).val());
            else
                titleObj.text("...");
        });

        $(".exobCategoryBlock").off();
        $(".exobCategoryBlock").on("click", function() {
            if($(this).next().hasClass("hide")) {
                $(".exobCategoryOptions").addClass("hide");
                $(this).next().removeClass("hide");
            } else $(".exobCategoryOptions").addClass("hide");
        });

        $(".removeAttr").off();
        $(".removeAttr").click(function (e) {
            e.preventDefault();

            if ($(this).parents(".categorySelectBox").children().length > 2)
                $(this).parents(".exobCategoryObj").remove();
            else {
                $(this).parents(".exobCategoryObj").find(".exobCategoryTitle").text("...");
                $(this).parents(".exobCategoryObj").find("input").val("");
                $(this).parents(".exobCategoryObj").find("select").val("");
            }
        });
    }

    $("#exobSaveGlobalSettings").click(function (e) {
        e.preventDefault();

        $("#exobSaveGlobalSettings").attr("disabled", "disabled");

        let randomAuthors = [];
        $(".authorsList.selectedAuthors > div").each(function(ind, obj) {
            randomAuthors.push($(obj).find(".authorNickname").attr("idAuthor"));
        });
        const data = {
            action: "exob_saveGlobalSettings",
            metaSourceNameKey: $("input[name=metaSourceNameKey]").val().trim(),
            metaSourceUrlKey: $("input[name=metaSourceUrlKey]").val().trim(),
            postsAuthor: $("select[name=postAuthor]").val(),
            randomAuthor: $("#postAuthorRandomCheckbox").is(":checked"),
            randomAuthors: randomAuthors
        };
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: data,
            success: function (data) {
                if (data === "success") {
                    $("#exobSaveGlobalSettings").removeAttr('disabled');
                    $(".successSave").fadeIn(400, function () {
                        setTimeout(function () {
                            $(".successSave").fadeOut(400);
                        }, 10000);
                    });
                }
            }
        });
    });
});
