=== EXOB ===
Contributors: Obroten54
Tags: EXOB
Requires at least: 4.8
Tested up to: 5.1.1
Stable tag: 0.1
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

EXOB - Exchange Posts between wp sites and get posts from Central Parser (private program)

== Description ==
EXOB give opportunity exchange posts between wp sites and get posts from Central Parser (private program on Java, coming soon...)

== Installation ==
1. Extract exob.zip
1. Upload the plugin folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the \'Plugins\' menu in WordPress

== Screenshots ==
1. EXIF metadata in readonly form fields (Insert Media Screen)
2. The attachment description in Twenty Seventeen
3. An archive of JPEG images

== Changelog ==
= 0.1 =
* Initial plugin release